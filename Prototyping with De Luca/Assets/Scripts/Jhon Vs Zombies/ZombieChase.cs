using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class ZombieChase : MonoBehaviour
{
    public GameObject Player;
    
    public float Speed;

    private float Distance;

    public AudioSource PlayerDeath;

    private Score Score;



    // Start is called before the first frame update
    void Start()
    {
        Score = FindObjectOfType<Score>();
    }

    // Update is called once per frame
    void Update()
    {   
        //Script that makes tho zombie go in the direction of the player
        Distance= Vector2.Distance(transform.position, Player.transform.position);
        Vector2 Direction = Player.transform.position - transform.position;
        
        //Script that makes the zombie look in the direction he's going
        if(this.transform.position.x < Player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0 , 0, 0);
        }
        else if (this.transform.position.x > Player.transform.position.x) 
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        transform.position = Vector2.MoveTowards(this.transform.position, Player.transform.position, Speed*Time.deltaTime);

        


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

       
        
            if (collision.gameObject.tag == "Sword")
            {
                 if (GameManager.instance.IsRotating)
                 {
                    ZombieDeath();
                 }
            }
        


        if (collision.gameObject.tag == "Player")
        {
            PlayerDeath.Play();
            GameManager.instance.Invoke("playerDeath", 1.5f);
            
        }
    }

    void ZombieDeath()
    {
        GameManager.instance.ZombieDying.Play();
        Score.ScoreUp();
        Destroy(gameObject);
        
    }

    
}
