using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class JhonMovement : MonoBehaviour
{

    public float Speed;

    public GameObject TopBoundary;
    public GameObject BotBoundary;
    public GameObject RightBoundary;
    public GameObject LeftBoundary;

    public GameObject JhonSprite;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Input.GetAxis("Horizontal") * Time.deltaTime * Speed, 0, 0);
        if (transform.position.x < LeftBoundary.transform.position.x + 0.9f)
        {
            transform.position = new Vector3(LeftBoundary.transform.position.x + 0.9f, transform.position.y, 0);
        }
        if (transform.position.x > RightBoundary.transform.position.x - 0.9f)
        {
            transform.position = new Vector3(RightBoundary.transform.position.x - 0.9f, transform.position.y, 0);
        }


        transform.Translate(0, Input.GetAxis("Vertical") * Time.deltaTime * Speed, 0);
        if (transform.position.y < BotBoundary.transform.position.y + 1)
        {
            transform.position = new Vector3(transform.position.x, BotBoundary.transform.position.y + 1, 0);
        }
        if (transform.position.y > TopBoundary.transform.position.y - 1)
        {
            transform.position = new Vector3(transform.position.x, TopBoundary.transform.position.y - 1, 0);
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
           JhonSprite.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
           JhonSprite.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

    }
}
