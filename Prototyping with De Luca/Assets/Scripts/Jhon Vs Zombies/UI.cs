using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UI : MonoBehaviour
{
    public GameObject PauseMenu;
    //public TextMeshProUGUI ScoreText;
    //private int Score;
    private void Start()
    {
        PauseMenu.SetActive(false);
    }

    private void Update()
    {
        //ScoreText.text = "KILLS: " + Score;

        if (PauseMenu.activeSelf == false)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {                
                PauseGame();
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {                
                ResumeGame();
            }
        }

        
    }

    public void StartNewGame()
    {
        SceneManager.LoadScene("Jhon Vs Zombies");
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    //public void ScoreUp()
    //{
        //Score++;
    //}
}
