using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class SwordParent : MonoBehaviour
{
    private Quaternion previousRotation;
    public bool IsRotating = false;
    // Start is called before the first frame update
    void Start()
    {
        previousRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        //Script that gets the pointer of the mouse and rotates thet sword in that direction
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        difference.Normalize();
        float rotation_z = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotation_z);

        
    }


    private void FixedUpdate()
    {
        //script that checks if the sword is moving
        if (transform.rotation != previousRotation)
        {
            GameManager.instance.IsRotating = true;
            Debug.Log("Is Rotating");
        }
        else if (transform.rotation == previousRotation)
        {
            GameManager.instance.IsRotating = false;
            Debug.Log("Is NOT Rotating");
        }
        previousRotation = transform.rotation;

        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print(IsRotating);
        //if (IsRotating == true)
        //{
            
       // }

    }
}
