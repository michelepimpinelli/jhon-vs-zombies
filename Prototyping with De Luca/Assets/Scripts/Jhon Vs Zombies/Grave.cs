using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grave : MonoBehaviour
{
    public float MinTimeToSpawn;
    public float MaxTimeToSpawn;
    float Timer;

    public GameObject Zombie;

    public GameObject grave;
    // Start is called before the first frame update
    void Start()
    {
        Timer = Random.Range(MinTimeToSpawn, MaxTimeToSpawn);
    }

    // Update is called once per frame
    void Update()
    {
        if (Timer >= 0)
        {
            Timer -= Time.deltaTime;
        }
        else
            Spawn();
    }

    void Spawn()
    {
        
        Instantiate(Zombie, grave.transform.position, transform.rotation);
        Timer = Random.Range(MinTimeToSpawn, MaxTimeToSpawn);
    }
}
