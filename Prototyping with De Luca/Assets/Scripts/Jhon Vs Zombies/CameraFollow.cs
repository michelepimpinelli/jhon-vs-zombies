using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject Jhon;

    private Vector3 offset;
    private Vector3 targetPosition;
    //public float cameraLag;

    // Start is called before the first frame update
    private void Start()
    {
        offset = transform.position - Jhon.transform.position;
    }

    // Update is called once per frame
    private void Update()
    {
        //targetPosition = Jhon.transform.position + offset;
        transform.position = Jhon.transform.position + offset;
    }

    private void FixedUpdate()
    {
        //transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * cameraLag);

    }
}
